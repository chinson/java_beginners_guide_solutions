package com.charles;

public class ch9_1 {
    public static class QueueFullException extends Exception {
        int size;

        QueueFullException (int s) { size = s; }

        @Override
        public String toString() {
            return "\nQueue is full. Maximum size is " + size;
        }
    }

    public static class QueueEmptyException extends Exception {
        @Override
        public String toString() {
            return "\nQueue is empty.";
        }
    }
}
