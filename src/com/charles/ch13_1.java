package com.charles;

public class ch13_1 {

    public class QueueFullException extends Exception {

        int size;

        QueueFullException(int size) {this.size = size;}

        @Override
        public String toString() {
            return "\nQueue is full. Maximum size is : " + size;
        }
    }

    public class QueueEmptyException extends Exception {
        @Override
        public String toString() {
            return "\nQueue is empty.";
        }
    }

    // a generic queue interface
    public interface IGenQ<T> {
        void put(T obj) throws QueueFullException;
        T get() throws QueueEmptyException;
    }

    public class GenQueue<T> implements IGenQ<T> {

        private T[] q; // this array holds the queue
        private int putloc, getloc; // the put and get indices

        // Construct an empty queue with the given array
        public GenQueue(T[] aRef) {
            q = aRef;
            putloc = getloc = 0;
        }

        @Override
        public void put(T obj) throws QueueFullException {
            if (putloc == q.length)
                throw new QueueFullException(q.length);
            q[putloc++] = obj;
        }

        @Override
        public T get() throws QueueEmptyException {
            if (getloc == putloc)
                throw new QueueEmptyException();
            return q[getloc++];
        }
    }

}
