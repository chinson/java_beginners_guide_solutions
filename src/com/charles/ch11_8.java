package com.charles;

import static java.lang.Thread.*;

public class ch11_8 {
    public static class TickTock{
        String state; // Contains the state of the clock

        synchronized void tick(boolean running) {
            if (!running) {
                // Stop the clock
                state = "ticked";
                notify();
                return;
            }
            System.out.print("Tick ");
            state = "ticked";
            notify(); // Let tock() run
            try {
                sleep(500);
                while (!state.equals("tocked"))
                    wait(); // wait for tock() to complete
            } catch (InterruptedException e) {
                System.out.println("Thread Interrupted");
                e.printStackTrace();
            }
        }

        synchronized void tock(boolean running) {
            if (!running) {
                // Stop the clock
                state = "tocked";
                notify();
                return;
            }
            System.out.println("Tock ");
            state = "tocked";
            notify();
            try {
                sleep(500);
                while (!state.equals("ticked"))
                    wait(); // wait for tick() to complete
            } catch (InterruptedException e) {
                System.out.println("Thread Interrupted");
                e.printStackTrace();
            }
        }
    }

    static class MyThread implements Runnable {

        Thread thread;
        TickTock ttOb;

        MyThread(String name, TickTock tt) {
            thread = new Thread(this, name);
            ttOb = tt;
        }

        // A factory method that creates and starts a thread
        public static MyThread createAndStart(String name, TickTock tt) {
            MyThread myThread = new MyThread(name, tt);
            myThread.thread.start();
            return myThread;
        }

        @Override
        public void run() {
            if (thread.getName().compareTo("Tick") == 0) {
                for (int i = 0; i < 5; i++) ttOb.tick(true);
                ttOb.tick(false);
            } else {
                for (int i = 0; i < 5; i++) ttOb.tock(true);
                ttOb.tock(false);
            }
        }
    }

//    static class ThreadCom {
//        public static void main(String[] args) {
//            TickTock tt = new TickTock();
//            MyThread mt1 = MyThread.createAndStart("Tick", tt);
//            MyThread mt2 = MyThread.createAndStart("Tock", tt);
//
//            try {
//                mt1.thread.join();
//                mt2.thread.join();
//            } catch (InterruptedException e) {
//                System.out.println("Main Thread Interrupted");
//                e.printStackTrace();
//            }
//        }
//    }
}
