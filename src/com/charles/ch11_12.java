package com.charles;

import static com.charles.ch8_1.ICharQ;
import static com.charles.ch9_1.QueueEmptyException;

public class ch11_12 {
    // Adds synchronization to the queue class
    static class SyncdDynCharQueue implements ICharQ {
        // A dynamic queue
        private char q[]; // The array that holds the queue
        private int putloc, getloc; // The put and get indicies

        public SyncdDynCharQueue(int size) {
            q = new char[size];
            putloc = getloc = 0;
        }

        @Override
        synchronized public void put(char ch) {
            if (putloc == q.length) {
                // increase the queue size
                char[] t = new char[q.length * 2];
                // copy elements into new queue
                for (int i = 0; i < q.length; i++)
                    t[i] = q[i];

                q = t;
            }
            q[putloc++] = ch;
        }

        @Override
        synchronized public char get() throws QueueEmptyException {
            if (getloc == putloc) {
                throw new QueueEmptyException();
            }
            return q[getloc++];
        }
    }

}
