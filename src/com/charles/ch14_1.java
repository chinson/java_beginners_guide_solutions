package com.charles;

public class ch14_1 {
    interface StringFunc {
        String func(String str);
    }

    public static String changeString(StringFunc sf, String s) {
        return sf.func(s);
    }

    public static void demo() {
        String inString = "Lambda expressions expand java";
        String outString;

        System.out.println("Original : " + inString);

        StringFunc reverse = str -> {
            String result = "";
            for (int i = str.length()-1; i>=0; i--)
                result+=str.charAt(i);
            return result;
        };

        outString = changeString(reverse, inString);
        System.out.println("New : " + outString);

        outString = changeString(str -> str.replace(' ', '-'), inString);
        System.out.println("Another : " + outString);

    }
}
