package com.charles;

public class ch6_6 {
    // Ch 6 self test 6
    public static String stringReverse(String s) {
        // Reverse s using recursion
        if (s.length() == 1) return s;
        return s.charAt(s.length()-1) + stringReverse(s.substring(0, s.length()-1));
    }
}
