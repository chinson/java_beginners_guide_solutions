package com.charles;

public class ch6_2 {
    // Further improving the queue class
    class Queue {
        private char q[]; // The array that holds the queue
        private int putloc, getloc; // The put and get indicies

        public Queue(int size) {
            q = new char[size];
            putloc = getloc = 0;
        }

        public Queue(Queue ob) {
            putloc = ob.putloc;
            getloc = ob.getloc;
            q = new char[ob.q.length];
            // copy elements
            for (int i = getloc; i < putloc; i++)
                q[i] = ob.q[i];
        }

        // Construct a Queue with initial values
        public Queue(char a[]) {
            putloc = getloc = 0;
            q = new char[a.length];
            for (int i = 0; i < a.length; i++) put(a[i]);
        }

        public void put(char ch) {
            if (putloc == q.length) {
                System.out.println(" - Queue is full");
                return;
            }
            q[putloc++] = ch;
        }

        public char get() {
            if (getloc == putloc) {
                System.out.println(" - Queue is empty");
                return (char) 0;
            }
            return q[getloc++];
        }
    }
}
