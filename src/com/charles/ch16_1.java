package com.charles;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.io.*;

public class ch16_1 {

    public static class SwingFC implements ActionListener {

        JTextField jtfFirst; // Holds the first file name
        JTextField jtfSecond; // Holds the second file name
        JButton jbtnComp; // Button to compare the files
        JLabel jlabFirst, jlabSecond; // Display prompts
        JLabel jlabResult; // Displays results and error messages
        // Added for 16.15
        JCheckBox jchShowMismatchPos;
        boolean showMismatchPos = false;

        SwingFC() {

            // Create a new JFrame container
            JFrame jfrm = new JFrame("Compare Files");

            // Specify FlowLayout for the layout manager
            jfrm.setLayout(new FlowLayout());

            // Give the frame an initial size
            jfrm.setSize(200, 190);

            // Terminate the program when the user closes the application
            jfrm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

            // Create the text fields for the file names
            jtfFirst = new JTextField(14);
            jtfSecond = new JTextField(14);

            // Set the action commands for the text fields
            jtfFirst.setActionCommand("fileA");
            jtfSecond.setActionCommand("fileB");

            // Create the compare button
            jbtnComp = new JButton("Compare");

            // Add action listener for the compare button
            jbtnComp.addActionListener(this::actionPerformed);

            // Create the labels
            jlabFirst = new JLabel("First file : ");
            jlabSecond = new JLabel("Second file : ");
            jlabResult = new JLabel("");

            // 16.15
            jchShowMismatchPos = new JCheckBox("Show position of mismatch");
            jchShowMismatchPos.addItemListener(this::itemStateChanged);

            // Add the components to the content frame
            jfrm.add(jlabFirst);
            jfrm.add(jtfFirst);
            jfrm.add(jlabSecond);
            jfrm.add(jtfSecond);
            jfrm.add(jbtnComp);
            jfrm.add(jlabResult);
            // 16.15
            jfrm.add(jchShowMismatchPos);

            // Display the frame
            jfrm.setVisible(true);
        }

        public void itemStateChanged(ItemEvent ie) {
            JCheckBox cb = (JCheckBox) ie.getItem();
            if (cb.isSelected()) {
                showMismatchPos = !showMismatchPos;
            }
        }

        // Compare the files when the compare button is pressed
        @Override
        public void actionPerformed(ActionEvent e) {

            String i = "";
            String j = "";
            // 16.15
            int lineCount;

            // First, confirm that both file names have been entered
            if (jtfFirst.getText().equals("")) {
                jlabResult.setText("First file name is missing.");
                return;
            }
            if (jtfSecond.getText().equals("")) {
                jlabResult.setText("Second file name is missing.");
                return;
            }

            // 16.15
            // Compare the files
            try (BufferedReader f1 = new BufferedReader(new FileReader(jtfFirst.getText()));
                 BufferedReader f2 = new BufferedReader(new FileReader(jtfSecond.getText())) ) {

                lineCount = 0;
                while ((i = f1.readLine()) != null && (j = f2.readLine()) != null) {
                    if (!i.equals(j)) break;
                    lineCount++;
                }

                if (!i.equals(j)) {
                    jlabResult.setText("Files are not the same");
                    if (showMismatchPos) {
                        jlabResult.setText(jlabResult.getText() + ", Mismatch at line " + (lineCount + 1)  );
                    }
                } else {
                    jlabResult.setText("Files are the same");
                }

            } catch (IOException ex) {
                jlabResult.setText("File Error");
                ex.printStackTrace();
            }
        }

        public static void main(String[] args) {
            // Create the frame on the event dispatching thread
            SwingUtilities.invokeLater(()->new SwingFC());
        }
    }

}
