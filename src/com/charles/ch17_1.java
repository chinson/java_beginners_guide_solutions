package com.charles;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;

public class ch17_1 {

    public static class CheckboxDemo extends Application {

        CheckBox cbSmartphone;
        CheckBox cbTablet;
        CheckBox cbNotebook;
        CheckBox cbDesktop;

        Label response;
        Label selected;

        String computers;

        public static void main(String[] args) {
            launch(args);
        }

        @Override
        public void start(Stage primaryStage) throws Exception {

            primaryStage.setTitle("Demonstrate Check Boxes");

            FlowPane rootNode = new FlowPane(Orientation.VERTICAL,10, 10);
            rootNode.setAlignment(Pos.CENTER);

            Scene myScene = new Scene(rootNode, 230, 200);

            primaryStage.setScene(myScene);

            Label heading = new Label("What computers do you own?");
            response = new Label("");
            selected = new Label("");

            cbSmartphone = new CheckBox("Smartphone");
            cbTablet = new CheckBox("Tablet");
            cbNotebook = new CheckBox("Notebook");
            cbDesktop = new CheckBox("Desktop");

            cbSmartphone.setOnAction(this::handleCheckbox);
            cbTablet.setOnAction(this::handleCheckbox);
            cbNotebook.setOnAction(this::handleCheckbox);
            cbDesktop.setOnAction(this::handleCheckbox);

            cbSmartphone.setAllowIndeterminate(true);
            cbTablet.setAllowIndeterminate(true);
            cbNotebook.setAllowIndeterminate(true);
            cbDesktop.setAllowIndeterminate(true);

            cbSmartphone.setIndeterminate(true);
            cbTablet.setIndeterminate(true);
            cbNotebook.setIndeterminate(true);
            cbDesktop.setIndeterminate(true);

            rootNode.getChildren().addAll(heading, cbSmartphone, cbTablet, cbNotebook, cbDesktop, response, selected);

            primaryStage.show();

            showAll();
        }

        private void handleCheckbox(ActionEvent ae) {
            CheckBox cb = (CheckBox) ae.getSource(); // Try this
            if (cb.isSelected())
                response.setText(cb.getText() + " was just selected");
            else if (cb.isIndeterminate())
                response.setText(cb.getText() + " is indeterminate");
            else
                response.setText(cb.getText() + " was just cleared");
            showAll();
        }

        private void showAll() {

            computers = "";
            if (cbSmartphone.isSelected()) computers = "Smartphone ";
            if (cbTablet.isSelected()) computers += "Tablet ";
            if (cbNotebook.isSelected()) computers += "Notebook ";
            if (cbDesktop.isSelected()) computers += "Desktop ";

            selected.setText("Computers selected : " + computers);
        }

        @Override
        public void stop() {

        }
    }

}
