package com.charles;

import java.io.FileInputStream;
import java.io.IOException;

public class ch10_1 {

    public static boolean compareFiles(String path1, String path2) {

        int i = 0;
        int j = 0;

        try (FileInputStream f1 = new FileInputStream(path1);
             FileInputStream f2 = new FileInputStream(path2)) {

            do {
                i = f1.read();
                j = f2.read();
                if (i != j) return false;
            } while (i != -1 && j != -1);

        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return true;
    }

}
