package com.charles;

public class ch6_1 {
    // Improving the Queue class
    class Queue {
        private char q[]; // The array that holds the queue
        private int putloc, getloc; // The put and get indicies

        public Queue(int size) {
            q = new char[size];
            putloc = getloc = 0;
        }

        public void put(char ch) {
            if (putloc == q.length) {
                System.out.println(" - Queue is full");
                return;
            }
            q[putloc++] = ch;
        }

        public char get() {
            if (getloc == putloc) {
                System.out.println(" - Queue is empty");
                return (char) 0;
            }
            return q[getloc++];
        }
    }
}
