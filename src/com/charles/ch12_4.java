package com.charles;

// improve upon ch12_1
public class ch12_4 {
    enum TrafficLightColor {
        RED(12000), GREEN(10000), YELLOW(2000);

        private int delay;

        TrafficLightColor(int delay) {
            this.delay = delay;
        }

        public int getDelay() {
            return delay;
        }

        public TrafficLightColor next() {
            return (this.ordinal() >= TrafficLightColor.values().length-1) ?
                    TrafficLightColor.values()[0] : TrafficLightColor.values()[this.ordinal() + 1];
        }
    }

    static class TrafficLightSimulator implements Runnable {

        private TrafficLightColor tlc;
        private boolean stop = false; // set to true to stop the simulation
        private boolean changed = false; // true when the light has changed

        TrafficLightSimulator(TrafficLightColor init) {
            tlc = init;
        }

        TrafficLightSimulator() {
            tlc = TrafficLightColor.RED;
        }

        @Override
        public void run() {
            while (!stop) {
                try {
                    Thread.sleep(tlc.getDelay());
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
                changeColor();
            }
        }

        synchronized void changeColor() {
            // change the light color
            tlc = tlc.next();
            changed = true;
            notify();
        }

        synchronized void waitForChange() {
            // wait until a light change occurs
            try {
                while (!changed)
                    wait();
                changed = false;
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }

        synchronized TrafficLightColor getColor() {
            return tlc;
        }

        synchronized void cancel() {
            // stop the simulation
            stop = true;
        }
    }

    static class TrafficLightDemo {
        public static void runDemo() {
            ch12_4.TrafficLightSimulator tl = new ch12_4.TrafficLightSimulator(TrafficLightColor.GREEN);
            Thread thread = new Thread(tl);
            thread.start();

            for (int i = 0; i < 10; i++) {
                System.out.println(tl.getColor());
                tl.waitForChange();
            }

            tl.cancel();
        }
    }
}
