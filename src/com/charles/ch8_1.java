package com.charles;

import com.charles.ch9_1.QueueFullException;
import com.charles.ch9_1.QueueEmptyException;

public class ch8_1 {
    // A character queue interface
    public interface ICharQ {
        void put(char ch) throws Exception;
        char get() throws Exception;
    }

    // A fixed queue implementation
    public class FixedQueue implements ICharQ {
        private char q[]; // The array that holds the queue
        private int putloc, getloc; // The put and get indicies

        public FixedQueue(int size) {
            q = new char[size];
            putloc = getloc = 0;
        }

        @Override
        public void put(char ch) throws QueueFullException {
            if (putloc == q.length) {
                throw new QueueFullException(q.length);
            }
            q[putloc++] = ch;
        }

        @Override
        public char get() throws QueueEmptyException {
            if (getloc == putloc) {
                throw new QueueEmptyException();
            }
            return q[getloc++];
        }
    }

    // A circular queue implementation
    public class CircularQueue implements ICharQ {
        private char q[]; // The array that holds the queue
        private int putloc, getloc; // The put and get indicies

        public CircularQueue(int size) {
            q = new char[size + 1];
            putloc = getloc = 0;
        }

        @Override
        public void put(char ch) throws QueueFullException {
            /* Queue is full if either putloc is one less than
            getloc, or if putloc is at the end of the array and
            getloc is at the beginning. */
            if (putloc + 1 == getloc | ((putloc==q.length-1) & (getloc == 0))) {
                throw new QueueFullException(q.length);
            }
            q[putloc++] = ch;
            if (putloc == q.length) putloc = 0; // loop back
        }

        @Override
        public char get() throws QueueEmptyException {
            if (getloc == putloc) {
                throw new QueueEmptyException();
            }
            char ch = q[getloc++];
            if (getloc == q.length) getloc = 0; // loop back
            return ch;
        }
    }

    // A dynamic queue
    public class DynQueue implements ICharQ {
        private char q[]; // The array that holds the queue
        private int putloc, getloc; // The put and get indicies

        public DynQueue(int size) {
            q = new char[size];
            putloc = getloc = 0;
        }

        @Override
        public void put(char ch) {
            if (putloc == q.length) {
                // increase the queue size
                char[] t = new char[q.length * 2];
                // copy elements into new queue
                for (int i = 0; i < q.length; i++)
                    t[i] = q[i];

                q = t;
            }
            q[putloc++] = ch;
        }

        @Override
        public char get() throws QueueEmptyException {
            if (getloc == putloc) {
                throw new QueueEmptyException();
            }
            return q[getloc++];
        }
    }
}
