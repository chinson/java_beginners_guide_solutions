package com.charles;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.effect.BoxBlur;
import javafx.scene.effect.Reflection;
import javafx.scene.layout.FlowPane;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Scale;
import javafx.stage.Stage;

// JavaFX Effects and Transforms Demo
public class ch17_2 {

    public static class EffectsAndTransformsDemo extends Application {

        double angle = 0.0;
        double scaleFactor = 0.4;
        double blurValue = 1.0;

        // Create initial effects and transformations:
        Reflection reflection = new Reflection();
        BoxBlur blur = new BoxBlur(1.0, 1.0, 1);
        Rotate rotate = new Rotate();
        Scale scale = new Scale(scaleFactor, scaleFactor);

        // Create push buttons
        Button btnRotate = new Button("Rotate");
        Button btnBlur = new Button("Blur");
        Button btnScale = new Button("Scale");

        Label reflect = new Label("Reflection adds visual sparkle");

        public static void main(String[] args) {
            launch(args);
        }

        @Override
        public void start(Stage primaryStage) throws Exception {

            primaryStage.setTitle("Effects and Transformations Demo");

            FlowPane rootNode = new FlowPane(20, 20);
            rootNode.setAlignment(Pos.CENTER);

            Scene myScene = new Scene(rootNode, 300, 120);

            primaryStage.setScene(myScene);

            btnRotate.getTransforms().add(rotate);
            btnScale.getTransforms().add(scale);

            reflection.setTopOpacity(0.7);
            reflection.setBottomOpacity(0.3);
            reflect.setEffect(reflection);

            btnRotate.setOnAction(ae -> {
                // Each time the button is pressed it is rotated 30 degrees around its center
                angle += 15;

                rotate.setAngle(angle);
                rotate.setPivotX(btnRotate.getWidth()/2.0);
                rotate.setPivotY(btnRotate.getHeight()/2.0);
            });

            btnScale.setOnAction(ae -> {
                // Each time the button is pressed its scale is changed
                scaleFactor += 0.1;
                if (scaleFactor > 2.0) scaleFactor = 0.4; // reset to initial value

                scale.setX(scaleFactor);
                scale.setY(scaleFactor);
            });

            btnBlur.setOnAction(ae -> {
                // Each time the button is pressed its blur status is changed
                if (blurValue == 10.0) {
                    blurValue = 1.0; // reset to initial value
                    btnBlur.setEffect(null); // remove the effect
                    btnBlur.setText("Blur off");
                } else {
                    blurValue++;
                    btnBlur.setEffect(blur);
                    btnBlur.setText("Blur on");
                }
                blur.setWidth(blurValue);
                blur.setHeight(blurValue);
            });

            rootNode.getChildren().addAll(btnRotate, btnScale, btnBlur, reflect);

            primaryStage.show();
        }
    }

}
