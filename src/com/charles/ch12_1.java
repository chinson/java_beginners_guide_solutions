package com.charles;

public class ch12_1 {
    enum TrafficLightColor {
        RED, GREEN, YELLOW
    }

    static class TrafficLightSimulator implements Runnable {

        private TrafficLightColor tlc;
        private boolean stop = false; // set to true to stop the simulation
        private boolean changed = false; // true when the light has changed

        TrafficLightSimulator(TrafficLightColor init) {
            tlc = init;
        }

        TrafficLightSimulator() {
            tlc = TrafficLightColor.RED;
        }

        @Override
        public void run() {
            while (!stop) {
                try {
                    switch (tlc) {
                        case GREEN:
                            Thread.sleep(10000); // Green for 10 seconds
                            break;
                        case YELLOW:
                            Thread.sleep(2000); // Yellow for 2 seconds
                            break;
                        case RED:
                            Thread.sleep(12000); // Red for 12 seconds
                            break;
                    }
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
                changeColor();
            }
        }

        synchronized void changeColor() {
            // change the light color
            switch (tlc) {
                case RED:
                    tlc = TrafficLightColor.GREEN;
                    break;
                case GREEN:
                    tlc = TrafficLightColor.YELLOW;
                    break;
                case YELLOW:
                    tlc = TrafficLightColor.RED;
                    break;
            }
            changed = true;
            notify();
        }

        synchronized void waitForChange() {
            // wait until a light change occurs
            try {
                while (!changed)
                    wait();
                changed = false;
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }

        synchronized TrafficLightColor getColor() {
            return tlc;
        }

        synchronized void cancel() {
            // stop the simulation
            stop = true;
        }
    }

    static class TrafficLightDemo {
        public static void runDemo() {
            TrafficLightSimulator tl = new TrafficLightSimulator(TrafficLightColor.GREEN);
            Thread thrd = new Thread(tl);
            thrd.start();

            for (int i = 0; i < 10; i++) {
                System.out.println(tl.getColor());
                tl.waitForChange();
            }

            tl.cancel();
        }
    }
}
