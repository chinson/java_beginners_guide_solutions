package userfuncs.binaryfuncs;


/* Defines a function that takes two int arguments
 * and returns an int result.  Thus, it can describe
 * any binary operation on two ints that returns an
 * int */
public interface BinaryFunc {

    // Obtain the name of the function
    String getName();

    /* This is the function to perform.
    It will be provided by specific implementations. */
    int func(int a, int b);
}
