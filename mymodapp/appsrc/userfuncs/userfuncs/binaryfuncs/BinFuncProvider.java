package userfuncs.binaryfuncs;

/* Defines the form of a service provider
 that obtains BinaryFunc instances */
public interface BinFuncProvider {
    // Obtain a BinaryFunc
    BinaryFunc get();
}
